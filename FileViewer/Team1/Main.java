import java.util.*;
import java.io.File;
import java.io.FileNotFoundException;

interface Element {
	public String getInfo();
}

class ScrollBar implements Element {
	@Override
	public String getInfo() {
		return "scrollBar";
	}
}

class ThickBlackBorder implements Element {
	@Override
	public String getInfo() {
		return "thickBlackBorder";
	}
}

class TextView {
	public String text;

	public List<Element> elements;

	public TextView() {
		elements = new ArrayList<>();
	}

	public void putText(String text) {
		this.text = text;
	}
	
	public void addElement(Element e) {
		this.elements.add(e);
	}

	public String display() {
		String res = this.text;
		for (Element e: elements) res += " " + e.getInfo();
		return res;
	}
}

public class Main {

	private Map<String, TextView> map;

	public Main() {
		map = new HashMap<>();
	}

	private Element createElement(String name) {
		if (name.equals("scrollBar")) {
			return new ScrollBar();
		}
		else if (name.equals("thickBlackBorder")) {
			return new ThickBlackBorder();
		}
		return null;
	}

	public void solve(String path) {
		try {
			File file = new File(path);
			Scanner scanner = new Scanner(file);

			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				String[] parts = line.split(" ");
				if (parts.length > 1) {
					String name = parts[0];
					String action = parts[1];
					
					TextView ptr;
					if (!map.containsKey(name)) {
						ptr = new TextView();
						map.put(name, ptr);
					}
					else ptr = map.get(name);

					if (action.equals("add")) {
						for (int i = 2; i < parts.length; i++)
							ptr.addElement(createElement(parts[i]));
					}
					else if (action.equals("display")) {
						System.out.println(ptr.display());
					}
					else {
						ptr.putText(action);
					}
				}
			}
		} catch (FileNotFoundException e) {
            System.err.println("File not found");
        }
    }
		
    public static void main(String[] args) {
		Main obj = new Main();
		obj.solve(args[0]);
	}
}

