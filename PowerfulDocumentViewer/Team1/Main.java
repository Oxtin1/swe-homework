import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public class Main {
	public static void main(String[] args) throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
    	PowerfulApplication powerfulApplication = new PowerfulApplication();
    	String fileName = "sampleInput";
    	File sample = new File(fileName);
        BufferedReader reader = new BufferedReader(new FileReader(sample));
        String line = reader.readLine();
        String[] Line;
        int i = 0;
        while (line != null) {
        	Line = line.split(" ");
        	if(Line[0].equals("Present"))
        		powerfulApplication.present();
        	else {
        		String clsName;
        		if(Line[0].equals("Draw"))
        			clsName = "DrawingDocument";
        		else
        			clsName = "TextDocument";
				Class cls = Class.forName(clsName);
				Document document = (Document) cls.getDeclaredConstructor().newInstance();
				powerfulApplication.create(document);
        	}
        	line = reader.readLine();
        }
	}
}

