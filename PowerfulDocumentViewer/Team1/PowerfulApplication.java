import java.util.ArrayList;

public class PowerfulApplication {
	ArrayList<Document> documents;
	
	public PowerfulApplication(){
		documents = new ArrayList<Document>();
	}
	
	public void present() {
		for(Document document: documents)
			document.display();
	}
	
	public void manage(Document document) {
		// manage document.
	}
	
	public void create(Document document) {
		documents.add(document);
	}
}
